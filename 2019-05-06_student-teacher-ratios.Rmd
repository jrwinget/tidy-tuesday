---
title: "#tidytuesday analysis: Global student-to-teacher ratios"
author: "J. Winget"
date: "Created: 2019-05-06, Updated: `r Sys.Date()`"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(tidyverse)
library(gghighlight)
library(patchwork)
theme_set(theme_minimal())
(ratios_raw <- read_csv("https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-05-07/student_teacher_ratio.csv"))
```

## Clean data

```{r}
(ratios <- ratios_raw %>% 
  select(-country_code) %>% 
  filter(!is.na(student_ratio)))
```

## Which countries have the highest and lowest median student-teacher ratios?

```{r}
ratios %>%
  group_by(country,
           indicator) %>% 
  summarize(med_ratio = median(student_ratio)) %>% 
  ungroup() %>% 
  top_n(50) %>% 
  mutate(country = fct_reorder(country, med_ratio)) %>% 
  ggplot(aes(country, med_ratio, color = indicator)) +
  geom_point() +
  coord_flip()

ratios %>%
  group_by(country,
           indicator) %>% 
  summarize(med_ratio = median(student_ratio)) %>% 
  ungroup() %>% 
  top_n(-50) %>% 
  mutate(country = fct_reorder(country, med_ratio, .desc = TRUE)) %>% 
  ggplot(aes(country, med_ratio, color = indicator)) +
  geom_point() +
  coord_flip()
```

## How do median ratios change over time based on indicator?

```{r}
palette <- c("#E69F00", "#56B4E9", "#009E73", "#F0E442",
             "#0072B2", "#D55E00", "#CC79A7", "#999999")

a <- ratios %>% 
  group_by(year,
           indicator) %>% 
  summarize(med_ratio = median(student_ratio)) %>% 
  ungroup() %>% 
  ggplot(aes(year, med_ratio, color = indicator)) +
  geom_line(size = 1.25) +
  expand_limits(y = 0) +
  scale_color_manual(values = palette) +
  labs(title = "How do median student-teacher ratios change over time?",
       x = "Year",
       y = "Median ratio",
       color = "Indicator")
```

```{r}
b <- ratios %>% 
  mutate(indicator = str_replace(indicator, 
                                 "Post-Secondary Non-Tertiary Education",
                                 "Post-Secondary Education")) %>% 
  ggplot(aes(year, student_ratio, color = indicator)) +
  geom_point() +
  geom_jitter(alpha = 2/3) +
  scale_color_manual(values = palette) +
  gghighlight(levels(indicator),
              use_direct_label = FALSE) +
  facet_wrap(~ indicator) + 
  stat_summary(fun.y = median, color = "black", geom = "point") +
  labs(x = "Year",
       y = "Median ratio",
       color = "Indicator",
       caption = "Source: UNESCO Institute of Statistics\nVisualization: @_jwinget")
```

## Final plot

```{r}
a + b + plot_layout(ncol = 1)
```
