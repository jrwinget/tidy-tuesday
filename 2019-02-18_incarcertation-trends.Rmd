---
title: "#tidytuesday analysis: Incarceration Trends"
author: "J. Winget"
date: "Created: 2019-02-18, Updated: `r Sys.Date()`"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

### Load packages and data

```{r}
library(tidyverse)
library(broom)

prison_pop <- read_csv(
  "https://raw.githubusercontent.com/rfordatascience/tidytuesday/master/data/2019/2019-01-22/prison_population.csv"
)
```

### Regressing population onto year for overall model

```{r}
# Perform linear regression of population by year: year_fit
year_fit <- lm(population ~ year, data = by_year_state)
summary(year_fit)
tidy(year_fit)
```

### Regressing population onto year for NY and TX

```{r}
# Perform linear regression of population by year for NY
ny_fit <- prison_pop %>%
  filter(state == "NY") %>% 
  lm(population ~ year, data = .)

# Fit model for TX
tx_fit <- prison_pop %>%
  filter(state == "TX") %>% 
  lm(population ~ year, data = .)

# Create tidied outputs
ny_tidied <- tidy(ny_fit)
tx_tidied <- tidy(tx_fit)

# Combine tidied models
bind_rows(ny_tidied, tx_tidied)
```

### Regressing population onto year for every state

```{r}
# Nest all columns besides state
nested <- prison_pop %>%
  nest(-state)

# Nested data for FL
nested$data[[10]]

# Perform linear regression on each item in the data column
(state_coef <- prison_pop %>%
  nest(-state) %>%
  mutate(
    model = map(data, ~ lm(population ~ year, data = .)),
    tidied = map(model, tidy)
  ) %>%
  unnest(tidied))
```

### Filter model terms and significant outcomes

```{r}
# Filter for slope terms, add adjusted p-values, and filter for significance
(filtered_states <- state_coef %>%
  filter(term == "year") %>%
  mutate(p.adj = p.adjust(p.value)) %>%
  filter(p.adjust(p.value) < .05))
```

### Sorting by slope

```{r}
# Sort for states with most quickly increasing incarceration trends
filtered_states %>%
  arrange(estimate)

# Sort for states with most quickly decreasing incarceration trends
filtered_states %>%
  arrange(desc(estimate))
```

### Graph the results

```{r}
filtered_states %>%
  mutate(
    state = fct_reorder(state, estimate)
  ) %>%
  ggplot() +
  aes(
    x = state,
    y = estimate,
    color = state
  ) +
  geom_point() +
  geom_hline(yintercept = 0,
             color = "black",
             size = 1) +
  geom_segment(aes(x = state,
                   xend = state,
                   y = estimate,
                   yend = 0)) +
  coord_flip() +
  labs(
    title = "Most quickly changing incarceration population trends (1970-2015)\n",
    x = "State\n",
    y = "\nChange in the number of individuals in the population"
  ) +
  theme_minimal() +
  theme(legend.position = "none")
```
